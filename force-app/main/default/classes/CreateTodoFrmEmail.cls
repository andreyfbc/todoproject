global class CreateTodoFrmEmail implements Messaging.InboundEmailHandler {


    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,
    Messaging.InboundEnvelope envelope) {

        String emailBody = email.PlainTextBody;
        String RecordText = 'Record Type: ';
        String TitleText = 'Title:';
        String PrText = 'Priority:';
        String StText = 'Status:';
        Integer rectype_startIndex = emailBody.indexOf(RecordText);
        Integer title_startIndex = emailBody.indexOf(TitleText);                                                 
        Integer pr_startIndex = emailBody.indexOf(PrText);
        Integer st_startIndex = emailBody.indexOf(StText);
        String RecordType = emailBody.substring(rectype_startIndex + RecordText.length(),title_startIndex-1);
        String Title = emailBody.substring(title_startIndex + TitleText.length(), pr_startIndex-1);
        String Priority = emailBody.substring(pr_startIndex + PrText.length(), st_startIndex-1);
        String Status = emailBody.substring(st_startIndex + StText.length());

        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();

		String subToCompare = 'Create Todo';

		if(email.subject.equalsIgnoreCase(subToCompare))
		{
			ToDo__c todo = new ToDo__c();
            if (RecordType.equalsIgnoreCase('Today')) {
                todo.RecordTypeId = '01209000000mQ1VAAU';
            } else if (RecordType.equalsIgnoreCase('Tomorrow')) {
                    todo.RecordTypeId = '01209000000mQ1aAAE';
                } else {
                    todo.RecordTypeId = '01209000000mQ1QAAU'; 
                }  
            todo.Name = Title;
            todo.Priority__c = Priority;
            todo.Status__c = Status;
			insert todo;
		}

	result.success = true;
        return result;
    }
}